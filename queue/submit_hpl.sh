#!/bin/bash
###### Job name ######
#PBS -N hpl_test
###### Output files ######
#PBS -o hpl.out
#PBS -e hpl.err
###### Number of nodes and cores ######
#PBS -l place=scatter
#PBS -l select=1:ncpus=2:mpiprocs=2
#PBS -l walltime=24:00:00
###### Queue name ######
#PBS -q workq
###### Sandbox ######
# PBS -W sandbox=PRIVATE
###### Specific the shell types ######
#PBS -S /bin/bash

###### Enter this job's working directory ######
cd $PBS_O_WORKDIR

###### Load modules to setup environment ######
. /etc/profile.d/modules.sh
module purge
module load mpi/openmpi3-x86_64

###### Run your jobs with parameters ######
if [ -n "$PBS_NODEFILE" ]; then
  if [ -f $PBS_NODEFILE ]; then
    NPROCS=`wc -l < $PBS_NODEFILE`
  fi
fi

export OMP_NUM_THREADS=1

mpirun -v -machinefile $PBS_NODEFILE -np $NPROCS ./xhpl