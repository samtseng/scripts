#version=DEVEL
# System authorization information
auth --enableshadow --passalgo=sha512
# Install OS instead of upgrade
install
# Use network installation
url --url="http://liho.tw/cobbler/ks_mirror/centos75-x86_64"
# Use text mode install
text
# Firewall configuration
firewall --disabled
# Run the Setup Agent on first boot
firstboot --enable
ignoredisk --only-use=sda
# Keyboard layouts
keyboard --vckeymap=us --xlayouts='us'
# System language
lang en_US.UTF-8

# Network information
network  --bootproto=dhcp --device=em1 --ipv6=auto --activate
network  --bootproto=dhcp --device=em2 --onboot=off --ipv6=off
network  --bootproto=dhcp --device=em3 --onboot=off --ipv6=off
network  --bootproto=dhcp --device=em4 --onboot=off --ipv6=off
#network  --bootproto=dhcp --hostname=localhost.localdomain

# Reboot after installation
eula --agreed
reboot

# Root password
rootpw --iscrypted $xxxxxxxxxxxxxxxxxxxxxxxx
# SELinux configuration
selinux --disabled
# System services
services --disabled="chronyd"
# Do not configure the X Window System
skipx

# System timezone
timezone Asia/Taipei --ntpservers=aruba.liho.tw,oak.liho.tw
# X Window System configuration information
# xconfig  --startxonboot

## System bootloader configuration
#bootloader --append=" crashkernel=auto" --location=mbr --boot-drive=sda
## Partition clearing information
##clearpart --all --initlabel --drives=sda
##clearpart --all --initlabel
#part swap --asprimary --fstype="swap" --size=8192
#part / --asprimary --fstype="xfs" --ondisk=sda --size=38146
#part /data --fstype="xfs" --ondisk=sda --size=1 --grow
#part swap --fstype="swap" --ondisk=sda --size=8192
#part /boot/efi --asprimary --fstype="efi" --ondisk=sda --size=238 --fsoptions="umask=0077,shortname=winnt"

# System bootloader configuration
bootloader --append=" crashkernel=auto" --location=mbr --boot-drive=sda
# Partition clearing information
clearpart --all --initlabel --drives=sda

part /boot --fstype ext4 --size=1024
part pv.01 --size=1000 --grow --ondisk=sda
volgroup vg00 pv.01
logvol /     --vgname=vg00 --fstype=xfs  --size=40960 --name=lv_root
logvol swap  --vgname=vg00 --fstype=swap --size=32768 --name=lv_swap
logvol /data --vgname=vg00 --fstype=xfs  --size=10000 --name=lv_data --grow


%pre
wipefs -a
%end

%post --logfile /root/ks-post.log

/usr/bin/lynx --source http://liho.tw/kickstart/postinstall-centos75-x86_64 | sh

%end

%packages
@^compute-node-environment
@base
@core
@additional-devel
@development
@debugging
@directory-client
@fonts
@java-platform
@network-file-system-client
@performance
@perl-runtime
@print-client
@ruby-runtime
@scientific
atlas
chrony
environment-modules
freeglut
freeglut-devel
kexec-tools
krb5-workstation
libXmu
libXp
libunistring
lynx
nscd
nss-pam-ldapd
numpy
openldap-clients
openmotif
pam_krb5
pam_ldap
perl-DBD-SQLite
python-matplotlib
scipy
tcl
gd-devel
libXpm-devel
hwloc-libs
nspr-devel
nss-util-devel
nss-softokn-devel
nss-softokn-freebl-devel
glibc.i686
glibc-static
perl-Switch
numactl
checkpolicy
audit-libs-python
python-IPy
openmpi3-devel
openmpi-devel
mpich-3.0-devel
infinipath-psm
libfabric
libpsm2
opensm-libs
blas-devel
lapack-devel
libogg-devel
libtheora-devel
pcre2
zziplib
texlive
python-devel
python-libs
python
qt5-qtbase-devel
glx-utils
pcre2-utf16
qt5-qtbase-common
qt5-qtbase-gui
qt5-rpm-macros
qt5-qtdeclarative-devel
qt5-qtxmlpatterns
qt5-qtsvg-devel
qt5-qttools-static
qt5-designer
qt5-doctools
qt5-linguist
qt5-qttools
qt5-qttools-common
qt5-qttools-devel
qt5-qttools-libs-designer
qt5-qttools-libs-designercomponents
qt5-qttools-libs-help
qt5-qtx11extras-devel
qt5-qtx11extras
xcb-util-image
xcb-util-keysyms
xcb-util-renderutil
xcb-util-wm
libX11-devel
emacs
expect
libyaml-devel
cairo-devel
libpng12
libseccomp-devel
squashfs-tools
rpm
rpm-build-libs
rpm-devel
rpm-libs
rpm-python
rpm-sign
rpm-build
git
perl-Error
perl-Git
perl-TermReadKey
compat-libgfortran-41
fftw-doc
fftw-devel
fftw-static
fftw-libs
fftw-libs-single
fftw-libs-long
fftw


%end

%addon com_redhat_kdump --enable --reserve-mb='auto'

%end

%anaconda
pwpolicy root --minlen=6 --minquality=50 --notstrict --nochanges --notempty
pwpolicy user --minlen=6 --minquality=50 --notstrict --nochanges --notempty
pwpolicy luks --minlen=6 --minquality=50 --notstrict --nochanges --notempty
%end
