#version=DEVEL
# System authorization information
auth --enableshadow --passalgo=sha512
# Install OS instead of upgrade
install
# Use network installation
url --url="http://192.168.1.254/cobbler/ks_mirror/centos7-x86_64"
# Use text mode install
text
# Firewall configuration
firewall --disabled
# Run the Setup Agent on first boot
firstboot --enable
ignoredisk --only-use=sda
# Keyboard layouts
keyboard --vckeymap=us --xlayouts='us'
# System language
lang en_US.UTF-8

# Network information
network  --bootproto=dhcp --device=enp0s3 --ipv6=auto --activate
network  --hostname=centos7

# Reboot after installation
eula --agreed
reboot

# Root password
rootpw --iscrypted $6$kGlAWQrLIxqXAf/K$X04JnBS/5OGHOWu0zesCaj9R5Q/4/bpAUqmtSjFKh2SCO6iRKWNZVwJVTPHEDoNZeeXugnlM15HG5A9AOSA/N.
# SELinux configuration
selinux --disabled
# System services
services --enabled="chronyd"
# Do not configure the X Window System
skipx

# System timezone
timezone Asia/Taipei --isUtc

# System bootloader configuration
bootloader --append=" crashkernel=auto" --location=mbr --boot-drive=sda
autopart --type=lvm
# Partition clearing information
clearpart --all --initlabel --drives=sda


%pre
wipefs -a
%end

%post --logfile /root/ks-post.log

/usr/bin/lynx --source http://192.168.1.254/kickstart/postinstall-centos7-x86_64 | sh

%end

%packages
@^compute-node-environment
@base
@core
@additional-devel
@development
chrony
kexec-tools
lynx
ypbind
yp-tools
nfs-utils
ntp
ntpdate
hwloc-libs
libX11-common
python3-setuptools
tcl
tk
libSM
libICE
environment-modules
blas
lapack
fftw
gsl
blas-devel
lapack-devel
fftw-devel
gsl-devel

%end

%addon com_redhat_kdump --enable --reserve-mb='auto'

%end

%anaconda
pwpolicy root --minlen=6 --minquality=1 --notstrict --nochanges --notempty
pwpolicy user --minlen=6 --minquality=1 --notstrict --nochanges --emptyok
pwpolicy luks --minlen=6 --minquality=1 --notstrict --nochanges --notempty
%end