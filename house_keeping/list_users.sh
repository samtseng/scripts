#!/bin/bash

function join { local IFS="$1"; shift; echo "$*"; }

USERS_LIST=`ypcat passwd | awk -F ":" '{ print $1","$3","$4","$6","$7 }'`

printf "%-8s\t%-7s\t%-8s\t%-8s\t%-8s\t%-8s\n" account uid group gid home shell
echo "-----------------------------------------------------------------------------------"

for i in ${USERS_LIST}
do
    USER_LIST_TMP=${i//,/ }
    USER=($USER_LIST_TMP)
    GID=`id -G ${USER[0]}`
    GID=${GID// /,}
    GROUP_LIST=`id ${USER[0]} | cut -d ' ' -f3`
    GROUP_LIST=`echo "${GROUP_LIST/groups=/}"`
    GROUP_LIST=${GROUP_LIST//,/ }
    GROUP_LIST=($GROUP_LIST)
    GROUP_ARR=()
    for j in ${GROUP_LIST[@]}
    do
        j=${j#*(}
        j=${j%)*}
        GROUP_ARR+=(${j})
    done
    GROUPS_LIST=$(join , ${GROUP_ARR[@]})

    printf "%-8s\t%-7s\t%-8s\t%-8s\t%-8s\t%-8s\n" ${USER[0]} ${USER[2]} $GROUPS_LIST ${GID} ${USER[3]} ${USER[4]}
done