#!/bin/bash

LOAD=`uptime | tr ',' ' ' | awk '{ print $10 }'`

flag=$(awk 'BEGIN{ print "'$LOAD'"<"'0.5'" }')

if [ "$flag" -eq 1 ]
then
    echo "clean /tmp"
    find /tmp -not -user root -not -user postgres -delete
fi